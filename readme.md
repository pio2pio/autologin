# Synopsis 

This script helps to login to LinuxAcademy servers and change default passwords.

# Supported LinuxAcademy OS
* Ubuntu 16 

# Usage
I'd recommend redirecting the STDOUT and STDERR to a text file
```
./autologin.sh ipaddress_or_hostname [password] &> scriptoutput.txt
```
# Record new login attempt, eg. new OS
```
autoexpect -f auto.exp ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null user@22.50.235.43
```

# First login atempt to Ubuntu 16, example text scroll:
```
ubuntu@ansiblebox-2:~/git/autologin$ ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null user@22.50.235.43
Warning: Permanently added '22.50.235.43' (ECDSA) to the list of known hosts.
user@22.50.235.43's password:
You are required to change your password immediately (root enforced)
Last login: Fri Jun 15 06:27:17 2018 from 11.214.11.13
WARNING: Your password has expired.
You must change your password now and login again!
Changing password for user user.
Changing password for user.
(current) UNIX password:
New password:
Retype new password:
passwd: all authentication tokens updated successfully.
Connection to 22.50.235.43 closed.
```
