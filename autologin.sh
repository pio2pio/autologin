#!/bin/bash 
# OS support: Ubuntu 16
# Usage     : ./autologin 22.33.44.55 [password123]
# REQUIRED  : sudo apt get install sshpass expect nmap
# EXIT CODES: 10 = No private or public files, 20 = Host is unavailable

   public_key="/home/$USER/.ssh/id_rsa.pub"
  private_key="/home/$USER/.ssh/id_rsa"
expect_script="./expect.exp"

SSHOPTS="-q -o StrictHostKeychecking=no -o UserKnownHostsFile=/dev/null"
PASS="${2:-piotrek123}" #assign $1 arg if exists, piotrek123 if it doesn't exits

#Change default remote password
sshpass -p "123456" ssh $SSHOPTS user@"$1" exit && ./auto.exp $1 || echo "OK Password has been already changed"
#./auto.exp $1

# check if both public and private file exist
if [ ! -f "$private_key" ] || [ ! -f "$public_key" ]
then
    echo "Either public or private key does not exist"; exit 10
else
    # check to see if host is available
    output=$(nmap "$1" -PN -p ssh | grep open)
    if [ ! -z "$output" ]
    then
        # attempt to connect to server and set up pubkey authentication
        sshpass -p "$PASS" ssh $SSHOPTS user@"$1" mkdir -m 700 .ssh
        if [ $? -eq 5 ]; then echo "ER Exit 5 Wrong password"; fi
        sshpass -p "$PASS" scp $SSHOPTS "$public_key" user@"$1":~/.ssh/authorized_keysa
        sshpass -p "$PASS" ssh $SSHOPTS user@"$1" chmod 400 .ssh/authorized_keys
    else
        echo "Host is not up or available"; exit 20
    fi
    ssh $SSHOPTS user@"$1" exit && echo "OK Connected sucessfully"|| echo "ER Cannot connect"
#    # call Expect script to move user to 'wheel' group
#    $expect_script "$private_key" "$1"
#
#    # disable password authentication
#    ssh -t -i "$private_key" user@"$1" "echo 123456 | sudo -S sed -i 's,^PasswordAuthentication yes,PasswordAuthentication no,' /etc/ssh/sshd_config"
#    ssh -t -i "$private_key"  user@"$1" "echo 123456 | sudo -S systemctl restart sshd.service"
fi
